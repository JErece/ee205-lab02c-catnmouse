###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02c - Cat 'N Mouse - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Cat 'N Mouse Program
###
### @author  Justin Erece <erece8@hawaii.edu>
### @date    25 January 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = catNmouse

all: $(TARGET)

catNmouse: catNmouse.c
	$(CC) $(CFLAGS) -o $(TARGET) catNmouse.c

clean:
	rm -f $(TARGET) *.o

