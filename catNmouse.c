///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  @todo yourName <@todo yourMail@hawaii.edu>
/// @date    @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>

#define DEFAULT_MAX_NUMBER (2048)

int main( int argc, char* argv[] ) 
{
//Declare variables
int randomNum() = rand() % DEFAULT_MAX_NUMBER + 1;
        
   printf( "Cat `n Mouse\n" );
   printf( "The number of arguments is: %d\n", argc );

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   int aGuess;
   printf( "Enter a number: " );
   scanf( "%d", &aGuess );
   printf( "The number was [%d]\n", aGuess );

   while( aGuess != randomNum)
   {
         printf( "OK Cat, I'm thinking of a number from 1 to [%d]. Make a guess\n", DEFAULT_MAX_NUMBER);
         scanf("%d", &aGuess);
   }

      //If user guesses a number less than 1
      if( aGuess < 1)
      {
         printf("You must enter a number that is >= 1\n");
      }

      //If user guesses number greater than DEFAULT_MAX_NUMBER
      else if( aGuess > DEFAULT_MAX_NUMBER )
      {
         printf("You must enter a number that is <= %d\n", DEFAULT_MAX_NUMBER);
      }

      //If user guesses number greater than random number
      else if( aGuess > randomNum)
      {
         printf("I'm thinking of a number smaller than %d\n", aGuess);
      }

      //If user guesses number greater than random number
      else if( aGuess < randomNum)
      {
         printf("I'm thinking of a number larger than %d\n", aGuess);
      }


      //If user guesses correctly
      else
      {
         printf("You got me\n");
         exit(0);
      }

   return 1;  // This is an example of how to return a 1
}

